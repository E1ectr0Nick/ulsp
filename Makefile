CC := gcc
PEG := minipeg
TARGETS := $(basename $(wildcard *.leg))
export PATH := minipeg:$(PATH)

all: minipeg generate

minipeg:
	make -C minipeg

generate: $(TARGETS)

%.c: %.leg
	$(PEG) -o $@ $<

watch:
	ls $(addsuffix .*, $(TARGETS)) | entr -c sh -c "make -s generate && make -s run"

run:
	-@cat $(word 1,$(TARGETS)).txt | ./$(word 1,$(TARGETS)) & sleep 1 && killall $(word 1,$(TARGETS))

clean:
	rm $(TARGETS)
