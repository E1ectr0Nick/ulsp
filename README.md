# Get and build

```
git clone --recursive https://gitlab.com/E1ectr0Nick/ulsp.git
cd ulsp
make
```

# Run

`make run` or `make watch` for live update.

